import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoinPage } from '../pages/loin/loin';
import { ContrackPage } from '../pages/contrack/contrack';
import { Map1Page } from '../pages/map1/map1';
import { Map2Page } from '../pages/map2/map2';
import { StoryPage } from '../pages/story/story';
import { TrackingPage } from '../pages/tracking/tracking';
import { BikerPage } from '../pages/biker/biker';
import { CommonModule } from '@angular/common';
import { FormsModule,   ReactiveFormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';

import { AgmCoreModule} from '@agm/core';
import { RegisterPage } from '../pages/register/register';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoinPage,
    ContrackPage,
    Map1Page,
    Map2Page,
    StoryPage,
    TrackingPage,
    BikerPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCsbPmvYcGfaGxhObUlSGZokh8VnCwbLbA',
      libraries: ["places"]
    }),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoinPage,
    ContrackPage,
    Map1Page,
    Map2Page,
    StoryPage,
    TrackingPage,
    BikerPage,
    RegisterPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
