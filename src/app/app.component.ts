import { Component, ViewChild } from '@angular/core';
import { Nav, Platform ,App } from 'ionic-angular';
//import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { LoinPage } from '../pages/loin/loin';
import { ContrackPage } from '../pages/contrack/contrack';
import { StoryPage } from '../pages/story/story';
import { TrackingPage } from '../pages/tracking/tracking';
import { BikerPage } from '../pages/biker/biker';
import * as firebase from 'Firebase';
import { Storage } from '@ionic/storage';
import { Map1Page } from '../pages/map1/map1';


const config = {
  apiKey: "AIzaSyCePubSmHajH_WCM3hlkpMDqYvpVM1zoMk",
  authDomain: "swe2018-1.firebaseapp.com",
  databaseURL: "https://swe2018-1.firebaseio.com",
  projectId: "swe2018-1",
  storageBucket: "swe2018-1.appspot.com",
  messagingSenderId: "861966881462"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoinPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen
    , private storage: Storage,
    private app:App) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'เรียก Bike and furious ', component: HomePage },
      { title: 'ข้อมูลส่วนตัว', component: StoryPage },
      { title: 'ติดตามสินค้า', component: TrackingPage },
      { title: 'ประวัติผู้ขับขี่', component: BikerPage },
      { title: 'ติดต่อ Bike and furious', component: ContrackPage },
    ];
  
   firebase.initializeApp(config);
  }

  initializeApp() {
    this.storage.ready().then(() => {
      this.storage.get('username').then((val) => {
        console.log(val);
        if(val != null){
          this.rootPage = Map1Page; 
        }
        else{
          console.log("not login")
        }
      });
  
    });
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  logOut(){
    this.storage.clear();
    this.nav.setRoot(LoinPage)
 }
}
