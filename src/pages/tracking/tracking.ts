import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'Firebase';  
import { Storage } from '@ionic/storage';
/**
 * Generated class for the TrackingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tracking',
  templateUrl: 'tracking.html',
})
export class TrackingPage {
  user_data:any = ""
  user_name
  ionViewWillEnter(){
    this.storage.ready().then(() => {
      this.storage.get('user_profile').then((val) => {
        this.user_data = val;
        console.log(this.user_data)
        this.user_name = this.user_data.name + " " + this.user_data.lastname
        this.loadOrderLog()
      });
    
    });
  }

  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storage: Storage    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoryPage');
  }

  orderLog = [];
  loadOrderLog(){
    firebase.database().ref('order').orderByChild("uid").equalTo(this.user_data.uid)
    .once('value', resp => {
    this.orderLog = []
    this.orderLog = snapshotToArray(resp)
    this.orderLog =  this.orderLog.reverse()
    console.log(this.orderLog )
    });
  }

}


export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};