import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as firebase from 'Firebase'; 
/**
 * Generated class for the StoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-story',
  templateUrl: 'story.html',
})
export class StoryPage {
  user_data:any = ""
  user_name
  ionViewWillEnter(){
    this.storage.ready().then(() => {
      this.storage.get('user_profile').then((val) => {
        this.user_data = val;
        console.log(this.user_data)
        this.user_name = this.user_data.name + " " + this.user_data.lastname
        this.loadOrderLog()
      });
    
    });
  }

  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storage: Storage    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoryPage');
  }

  orderLog = [];
  loadOrderLog(){
    firebase.database().ref('order').orderByChild("uid").equalTo(this.user_data.uid)
    .once('value', resp => {
    this.orderLog = []
    this.orderLog = snapshotToArray(resp)
  
    });
  }

}


export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};