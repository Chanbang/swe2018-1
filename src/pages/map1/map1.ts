import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home'
import { MapsAPILoader } from '@agm/core';
//import {} from '@types/googlemaps';
import { ViewChild, ElementRef, NgZone } from '@angular/core';  
import { FormControl } from '@angular/forms'; 
import {} from '@types/googlemaps';  
import { } from 'googlemaps'; 
import { ModalController, ViewController } from 'ionic-angular';
/**
 * Generated class for the Map1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map1',
  templateUrl: 'map1.html',
})
export class Map1Page {

  search_place

  constructor(public navCtrl: NavController,
     public navParams: NavParams ,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public viewCtrl: ViewController) {

  }



  locationChosen = false;

  onChoseLocation(event) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.locationChosen = true;
  }

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;

  @ViewChild("search")
  public searchElementRef: ElementRef ;
 

  

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        
        
      });
    }
  }

  ngAfterViewInit() {
    console.log("after init")
    let input = document.getElementById('autocomplete').getElementsByTagName('input')[0];
       
    console.log(input)
    this.loadMap(input)
  //  var options = {componentRestrictions: {country: 'us'}};
    //new google.maps.places.Autocomplete(input, options);
    console.log("after end")
 
}


  goTohome(){
    this.navCtrl.setRoot(HomePage)
  }

  loadMap(input){
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(input, { 
      });
      console.log(autocomplete)
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
       
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          console.log(place.formatted_address) 
          this.search_place = place.formatted_address
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad Map1Page');
 
 
  }
  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(this.search_place);
  }
 
 

}
