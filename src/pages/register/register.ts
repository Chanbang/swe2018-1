import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import * as firebase from 'Firebase'; 
import { Md5 } from 'ts-md5/dist/md5';
import { LoinPage } from '../loin/loin';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  username
  password
  name
  lastname
  phone

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'ข้อมูลไม่ครบถ้วน',
      subTitle: 'กรุณากรอกข้อมูลให้ครบ',
      buttons: ['ตกลง']
    });
    alert.present();
  }

  registerAlert() {
    let alert = this.alertCtrl.create({
      title: 'สมัครสมาชิกเรียบร้อย',
      subTitle: 'ท่านสามารถเข้าสู่ระบบได้ทันที',
      buttons: ['ตกลง']
    });
    alert.present();
  }
  

  register(){
    if(this.username==null||
    this.password==null||
    this.name==null||
    this.lastname==null||
    this.phone==null
    ){
        this.presentAlert()
    }
    else{
      firebase.database().ref('users').orderByChild("username").equalTo(this.username)
      .once('value', resp => {
      if (resp.val()!=null){
        let alert = this.alertCtrl.create({
          title: 'ชื่อนี้มีผู้ใช้งานแล้ว',
          subTitle: 'กรุณาเปลี่ยนชื่อของท่าน',
          buttons: ['ตกลง']
        });
        alert.present();
      }
      else{
        console.log("regis")
        let time = new Date().getTime(); 
        let hash = Md5.hashStr(time.toString());//unique id
        let data = {
          username:this.username,
          password:this.password,
          name:this.name,
          lastname:this.lastname,
          phone:this.phone,
          uid:hash,
          point:500        }
        let register = firebase.database().ref('users/'+hash);
        register.update(data).then(()=>{
          this.registerAlert()
          this.navCtrl.setRoot(LoinPage)
        }
        ) 
      }
      });

    }
    };
  
}
