import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContrackPage } from './contrack';

@NgModule({
  declarations: [
    ContrackPage,
  ],
  imports: [
    IonicPageModule.forChild(ContrackPage),
  ],
})
export class ContrackPageModule {}
