import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { Map1Page } from '../map1/map1';
import { Map2Page } from '../map2/map2'
import { ModalController } from 'ionic-angular';
import * as firebase from 'Firebase';
import { Storage } from '@ionic/storage';
import { TrackingPage } from '../tracking/tracking';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private alertCtrl: AlertController) {

  }

  place_start
  place_destination
  place_start_note
  place_destination_note

  sender_name
  sender_phone
  receiver_name
  receiver_phone
  user_point

  service_type = "เอกสารทั่วไป"
  note

  user_data
  user_point_use = 0
  transport_price = 110

  test(){
    let time = new Date().getTimezoneOffset(); 
    console.log(time)
  }

  ionViewWillEnter(){
    this.storage.ready().then(() => {
      this.storage.get('user_profile').then((val) => {
        this.user_data = val;
        console.log(this.user_data)
        this.sender_name = this.user_data.name + " " + this.user_data.lastname
        this.sender_phone = this.user_data.phone
      });
    
    });
  }

  presentConfirm() {

    let alert2 = this.alertCtrl.create({
      title: 'แต้มของคุณไม่เพียงพอ', 
      buttons: ['ตกลง']
    });
    if(  this.user_data.point/20 - this.user_point_use < 0  ){
      alert2.present();
      return
    }

    

    let alert = this.alertCtrl.create({
      title: 'ยืนยันการส่ง',
      message: 'ท่านต้องการยืนยันการส่งใช่หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ตกลง',
          handler: () => {
            this.confirmOrder()
          }
        }
      ]
    });
    alert.present();
  }
  confirmOrder(){
    
     let time = new Date(); 
     let order_id = new Date().getTime(); 
    console.log("confirm order")
    let data = {
      place_start: this.place_start,
      place_destination:this.place_destination,
      place_start_note:this.place_start_note,
      place_destination_note:this.place_destination_note, 
      sender_name:this.sender_name,
      sender_phone:this.sender_phone,
      receiver_name:this.receiver_name,
      receiver_phone:this.receiver_phone,
      service_type:this.service_type,
      note:this.note,
      uid:this.user_data.uid,
      order_time: time,
      order_id:order_id,
      discount:this.user_point_use,
      transport_price:this.transport_price,
      total_price: this.transport_price-this.user_point_use
    }
      console.log(data)
      let newData = firebase.database().ref('order').push();
      newData.update(data);

      let updatePoint = firebase.database().ref('users/'+this.user_data.uid);

      let current_point = (this.user_data.point/20 - this.user_point_use)*20
      updatePoint.update({point:current_point});
      this.user_data["point"]=current_point
      this.storage.set('user_profile',  this.user_data)


      let alert = this.alertCtrl.create({
        title: 'ได้รับการยืนยันการเรียกใช้บริการ',
        subTitle: 'กรุณารอทางเราติดต่อไปนะคะ',
        buttons: ['ตกลง']
      });
      alert.present()
      this.navCtrl.push(TrackingPage)
  }

  goTomap1(){
    this.navCtrl.push(Map1Page)
  }
  goTomap2(){
    this.navCtrl.push(Map2Page)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  
  }

  mapModal1() {
    console.log("start")
    let mapModal1 = this.modalCtrl.create(Map1Page);
    mapModal1.onDidDismiss(data => {
      console.log(data);
      this.place_start=data
      console.log(this.place_start)
    });
    mapModal1.present();


  }
 
  
  mapModal2() {
    console.log("destinaiton")

    let mapModal2 = this.modalCtrl.create(Map1Page);
    mapModal2.onDidDismiss(data => {
      console.log(data);
      this.place_destination=data
      console.log(this.place_destination)
    });
    mapModal2.present();


  }
 
}

