import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import { HomePage } from '../home/home'
import * as firebase from 'Firebase'; 
import { RegisterPage } from '../register/register'
import { Storage } from '@ionic/storage';
  
/**
 * Generated class for the LoinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loin',
  templateUrl: 'loin.html',
})
export class LoinPage {

  username
  password

  user_data = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController,
    private storage: Storage) {
  }

  ionViewWillEnter(){
 
    this.storage.ready().then(() => {
      this.storage.get('user_profile').then((val) => {
        console.log(val);
        if(val != null){
          this.navCtrl.setRoot(HomePage)
        }
      });
  
    });
    
  }



  goTomenu(){
    if(this.username==null||this.password==null){
        return
    }

    let alert = this.alertCtrl.create({
      title: 'ไม่สามารถเข้าสู่ระบบได้',
      subTitle: 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง',
      buttons: ['ตกลง']
    });


  
    firebase.database().ref('users').orderByChild("username").equalTo(this.username)
    .once('value', resp => {
    if (resp.val()!=null){ 
           this.user_data = [];
           this.user_data = snapshotToArray(resp); 
           console.log(resp.val()) 
           console.log(this.password)
           console.log(this.user_data)
           console.log(this.user_data[0].password)
           if(this.password==this.user_data[0].password){
            this.storage.set('user_profile',this.user_data[0])
            this.navCtrl.setRoot(HomePage)

           } 
           else{
            alert.present();
           }
    }
    else{
      console.log("not match")
      alert.present();
    }
    });
    //this.navCtrl.setRoot(HomePage)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoinPage');
  }
  goToregis(){


    this.navCtrl.push(RegisterPage)
  }


}


export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};