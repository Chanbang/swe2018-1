import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoinPage } from './loin';

@NgModule({
  declarations: [
    LoinPage,
  ],
  imports: [
    IonicPageModule.forChild(LoinPage),
  ],
})
export class LoinPageModule {}
